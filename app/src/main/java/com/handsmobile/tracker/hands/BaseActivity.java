package com.handsmobile.tracker.hands;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import butterknife.BindView;

/**
 * Created by aeciolima on 04/04/2018.
 */

public class BaseActivity extends AppCompatActivity {
    private ProgressDialog dialog;
    @BindView(R.id.progress)
    ProgressBar progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showLoader(boolean cancellable) {
        if(progress != null) {
            progress.setVisibility(View.VISIBLE);
            progress.setIndeterminate(true);
            return;
        }
        dialog = ProgressDialog.show(this, "", "Carregando...", true, cancellable);
    }

    public void hideLoader() {
        if(progress != null) {
            progress.setVisibility(View.GONE);
            return;
        }

        if(dialog != null) {
            dialog.dismiss();
        }
    }
}
