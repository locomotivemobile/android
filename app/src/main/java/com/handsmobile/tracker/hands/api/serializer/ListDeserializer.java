package com.handsmobile.tracker.hands.api.serializer;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class ListDeserializer<T> implements JsonDeserializer<T> {
    @Override public T deserialize(JsonElement json, Type type, JsonDeserializationContext context)
            throws JsonParseException {
        JsonArray rootObject = ((JsonObject) json).getAsJsonObject("data").getAsJsonArray("allTracks");
        return new Gson().fromJson(rootObject, type);
    }
}
