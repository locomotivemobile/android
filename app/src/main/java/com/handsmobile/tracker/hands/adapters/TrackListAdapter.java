package com.handsmobile.tracker.hands.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.handsmobile.tracker.hands.BaseActivity;
import com.handsmobile.tracker.hands.R;
import com.handsmobile.tracker.hands.api.model.Track;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class TrackListAdapter extends RecyclerView.Adapter<TrackListAdapter.TrackItemViewHolder> {
    private List<Track> list;
    private BaseActivity context;
    private final PublishSubject<Track> onClickSubject = PublishSubject.create();

    public TrackListAdapter(BaseActivity ctx, List<Track> items) {
        list = items;
        context = ctx;
    }

    @Override
    public TrackItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.track_list_item, viewGroup, false);

        TrackListAdapter.TrackItemViewHolder viewHolder = new TrackListAdapter.TrackItemViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TrackItemViewHolder viewHolder, int position) {
        final Track order = list.get(position);

        viewHolder.userId.setText(order.userId);
        viewHolder.deviceModel.setText(order.deviceModel);
        viewHolder.deviceOs.setText(order.deviceOs);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSubject.onNext(order);
            }
        });
    }

    public Observable<Track> getPositionClicks() {
        return onClickSubject;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class TrackItemViewHolder extends RecyclerView.ViewHolder {

        TextView userId;
        TextView deviceModel;
        TextView deviceOs;

        public TrackItemViewHolder(View itemView) {
            super(itemView);

            userId = itemView.findViewById(R.id.user_id);
            deviceModel = itemView.findViewById(R.id.device_model);
            deviceOs = itemView.findViewById(R.id.device_os);
        }
    }
}


