package com.handsmobile.tracker.hands.api.model;

/**
 * Created by aeciolima on 04/04/2018.
 */

public class Position {
    public static Position Builder(double lat, double lng) {
        return new Position(lat, lng);
    }

    public Position(double lat, double lng) {
        this.latitude = lat;
        this.longitude = lng;
    }

    public double latitude;
    public double longitude;
}
