package com.handsmobile.tracker.hands.api;

import android.app.Application;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.handsmobile.tracker.hands.R;
import com.handsmobile.tracker.hands.api.model.Track;
import com.handsmobile.tracker.hands.api.serializer.ListDeserializer;
import com.handsmobile.tracker.hands.api.serializer.TrackDeserializer;
import com.handsmobile.tracker.hands.api.service.ApiService;

import java.util.List;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpClient {
    private static String API_BASE_URL;

    private ApiService apiService;
    private Retrofit retrofit;

    public HttpClient(final Application app) {
        API_BASE_URL = app.getResources().getString(R.string.api_base_url);

//        final String authCredentials = app.getResources().getString(R.string.basic_auth_credentials);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client =
                new OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor(chain -> {
                    Request original = chain.request();

                    original = original.newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("User-Agent", "android")
                            .build();

                    return chain.proceed(original);
                }).build();

        Gson gson = new GsonBuilder().registerTypeHierarchyAdapter(List.class, new ListDeserializer<List<Track>>())
                .registerTypeAdapter(Track.class, new TrackDeserializer<Track>())
                .create();

        retrofit = new Retrofit.Builder().baseUrl(API_BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();

        apiService = retrofit.create(ApiService.class);
    }

    public ApiService getApiService() {
        return apiService;
    }
}