package com.handsmobile.tracker.hands;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.handsmobile.tracker.hands.api.model.Track;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aeciolima on 06/04/2018.
 */

public class DetailActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        showLoader(false);

        String id = getIntent().getExtras().getString("id");
        String url = String.format("graphql?query={getTrack(trackId:\"%s\"){userId,home{lat,lng},work{lat,lng},place{lat,lng},address,deviceModel,deviceOs}}", id);

        Call<Track> request = App.getRestClient().getApiService().getTrack(url);
        request.enqueue(new Callback<Track>() {
            @Override
            public void onResponse(Call<Track> call, Response<Track> response) {
                hideLoader();

                if(response.isSuccessful()) {

                }
            }

            @Override
            public void onFailure(Call<Track> call, Throwable t) {
                hideLoader();
            }
        });
    }
}
