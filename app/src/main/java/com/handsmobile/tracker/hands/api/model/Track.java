package com.handsmobile.tracker.hands.api.model;

/**
 * Created by aeciolima on 04/04/2018.
 */

public class Track extends ApiResponse {
    public static Track Builder(String id, String userId, String deviceModel, String deviceOs, String deviceOsVersion, String addr, Position home, Position work, Position place) {
        Track track = new Track();

        track.id = id;
        track.userId = userId;
        track.deviceModel = deviceModel;
        track.deviceOs = deviceOs;
        track.deviceOsVersion = deviceOsVersion;
        track.home = home;
        track.work = work;
        track.place = place;
        track.address = addr;

        return track;
    }

    public String id;
    public String userId;
    public String deviceOs;
    public String deviceModel;
    public String deviceOsVersion;
    public String address;
    public Position home;
    public Position work;
    public Position place;
}
