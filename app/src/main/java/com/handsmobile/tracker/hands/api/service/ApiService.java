package com.handsmobile.tracker.hands.api.service;

import com.handsmobile.tracker.hands.api.model.Track;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by aeciolima on 01/04/2018.
 */

public interface ApiService {
    @FormUrlEncoded
    @POST("graphql?query={addTrack{Track}}")
    Call<Track> addTrack(@Body Track track);

    @GET("graphql?query={allTracks{id,userId,deviceModel,deviceOs,deviceOsVersion,place{lat,lng}}}")
    Call<List<Track>> listTracks();

    @GET
    Call<Track> getTrack(@Url String id);
}