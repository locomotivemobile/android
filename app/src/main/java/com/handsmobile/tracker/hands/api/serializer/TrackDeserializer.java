package com.handsmobile.tracker.hands.api.serializer;

import com.google.gson.JsonDeserializer;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.handsmobile.tracker.hands.api.model.Track;

import java.lang.reflect.Type;

public class TrackDeserializer<T> implements JsonDeserializer<T> {
    @Override public T deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject root = json.getAsJsonObject().getAsJsonObject("data");
        Track track;

        try {
            track = new Gson().fromJson(root.getAsJsonObject().get("getTrack"), Track.class);
        } catch (Exception e) {
            return new Gson().fromJson(json.getAsJsonObject(), type);
        }

        return (T) track;
    }
}