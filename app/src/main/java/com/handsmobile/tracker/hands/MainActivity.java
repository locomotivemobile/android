package com.handsmobile.tracker.hands;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.handsmobile.tracker.hands.adapters.TrackListAdapter;
import com.handsmobile.tracker.hands.api.model.Track;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        showLoader(false);

        Call<List<Track>> history = App.getRestClient().getApiService().listTracks();
        history.enqueue(new Callback<List<Track>>() {
            @Override
            public void onResponse(Call<List<Track>> call, Response<List<Track>> response) {
                hideLoader();

                if(response.isSuccessful()) {
                    if(response.body().size() == 0) {
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        LinearLayoutManager layoutManager
                                = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);

                        TrackListAdapter adapter = new TrackListAdapter(MainActivity.this, response.body());

                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(adapter);
                        adapter.getPositionClicks().subscribe(track -> {
                            Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                            intent.putExtra("id", track.id);

                            startActivity(intent);
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Track>> call, Throwable t) {
                hideLoader();
            }
        });
    }
}
