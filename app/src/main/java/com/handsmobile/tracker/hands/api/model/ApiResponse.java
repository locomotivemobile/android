package com.handsmobile.tracker.hands.api.model;

import com.google.gson.annotations.SerializedName;

public class ApiResponse {
    public static final int ERROR_CODE_UNAUTHORIZED = 401;
    public static final String STATUS_OK = "OK";

    @SerializedName("status")
    public int statusCode;
    @SerializedName("apicode")
    public int apiCode;
    @SerializedName("message")
    public String message;
}
