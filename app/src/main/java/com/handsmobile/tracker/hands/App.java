package com.handsmobile.tracker.hands;

import android.app.Application;

import com.handsmobile.tracker.hands.api.HttpClient;

/**
 * Created by aeciolima on 01/04/2018.
 */

public class App extends Application {

    private static HttpClient restClient;

    @Override
    public void onCreate() {
        super.onCreate();

        restClient = new HttpClient(this);
    }

    public static HttpClient getRestClient() {
        return restClient;
    }
}
